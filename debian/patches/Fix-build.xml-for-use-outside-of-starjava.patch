From: Ole Streicher <olebole@debian.org>
Date: Thu, 16 Feb 2017 16:38:35 +0100
Subject: Fix build.xml for use outside of starjava

This includes the following changes:

 * Rename the `extclasspath` elements to `path`. With ant >= 1.6, there is no
   difference (and before, the difference was not relevant for Debian)

 * Ignore local property files

 * Change the starlink installation dir to the standard Java path /usr/share/java/

 * Prefix the name by `starlink-`

 * Adjust the build and test classpaths for Debian (also for CI tests)

 * Set a class path in the jar manifest

 * Set the source file encoding (cp1252), but not the source version (deprecated)

 * Don't sign the jarfile
---
 build.xml | 69 +++++++++++++++++++++++++++++----------------------------------
 1 file changed, 32 insertions(+), 37 deletions(-)

diff --git a/build.xml b/build.xml
index 903cc12..2ce9ec7 100644
--- a/build.xml
+++ b/build.xml
@@ -32,12 +32,6 @@
  !-->
 
 <project name="Build file for VOTable" default="build" basedir=".">
-
-  <!-- If either or both of these files exist then any properties
-   !   contained within them will override those defined here.  -->
-  <property file="${user.home}/.stardev.properties"/>
-  <property file=".properties"/>
-
   <!-- Properties will also be set for all environment variables
    !   (PATH becomes "env.PATH"), generally not a good
    !   idea as names are OS dependent -->
@@ -50,13 +44,13 @@
    !-->
 
   <!-- Directory for the Starlink installation (usually /star/java)-->
-  <property name="star.dir" value="${basedir}/../../"/>
+  <property name="star.dir" value="/usr/share/java"/>
 
   <!-- Directory to install into (install target, usually /star/java)-->
   <property name="star.install" value="${star.dir}"/>
 
   <!-- Directory that contains the Starlink jar tree -->
-  <property name="star.jar.dir" value="${star.dir}/lib"/>
+  <property name="star.jar.dir" value="${star.dir}"/>
 
   <!-- Directory that contains the locally built sources (usually
    !   /star/java/source for full distribution) -->
@@ -78,8 +72,8 @@
    !-->
 
   <!-- Define the package name and current versions -->
-  <property name="Name" value="VOTable"/>
-  <property name="name" value="votable"/>
+  <property name="Name" value="Starjava VOTable"/>
+  <property name="name" value="starlink-votable"/>
   <property name="version" value="2.0"/>
 
   <!-- The Java package name -->
@@ -194,14 +188,23 @@
    !   class files in preference to installed ones (an extra user-define
    !   defined CLASSPATH can also be used as needed).
    !-->
-  <extclasspath id="installed.classpath">
-
-    <!-- {Other packages} -->
-    <pathelement location="${star.jar.dir}/table/table.jar"/>
-    <pathelement location="${star.jar.dir}/util/util.jar"/>
-    <pathelement location="${star.jar.dir}/tamfits/tamfits.jar"/>
+  <path id="installed.classpath">
+    <pathelement location="${star.jar.dir}/starlink-fits.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-table.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-util.jar"/>
+    <pathelement location="${star.jar.dir}/axis-jaxrpc.jar"/>
+    <pathelement location="${star.jar.dir}/axis.jar"/>
+    <pathelement location="${star.jar.dir}/fits.jar"/>
+  </path>
 
-  </extclasspath>
+  <path id="jar.classpath">
+    <pathelement location="${dist.lib.pkg}/starlink-fits.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-table.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-util.jar"/>
+    <pathelement location="${dist.lib.pkg}/axis-jaxrpc.jar"/>
+    <pathelement location="${dist.lib.pkg}/fits.jar"/>
+    <pathelement location="${dist.lib.pkg}/axis.jar"/>
+  </path>
 
   <!-- Generate the local build classpath. This is the most difficult
    !   part of handling the classpath. The local classes will be in
@@ -219,13 +222,7 @@
    !   doesn't work as expected add the additional classes/jars to
    !   the extra classpath.
    !-->
-  <extclasspath id="built.jarpath">
-
-    <pathelement location="${star.build.dir}/table/lib/table/table.jar"/>
-    <pathelement location="${star.build.dir}/util/lib/util/util.jar"/>
-    <pathelement location="${star.build.dir}/tamfits/lib/tamfits/tamfits.jar"/>
-
-  </extclasspath>
+  <path id="built.jarpath"/>
 
   <path id="built.classpath">
 
@@ -266,11 +263,13 @@
   <!-- JUnit tests classpath, add tests.dir and tests.etc.dir so that
    !   resources may be located there -->
  <path id="tests-classpath">
+    <pathelement location="${star.jar.dir}/junit.jar"/>
     <pathelement location="${build.classes}"/>
     <pathelement location="${build.tests}"/>
     <pathelement location="${tests.dir}"/>
     <pathelement location="${tests.etc.dir}"/>
     <path refid="classpath"/>
+    <pathelement location="${star.jar.dir}/${name}.jar"/>
   </path>
 
   <!-- Turn this path into a string which is passed to the tests -->
@@ -359,7 +358,8 @@
            destdir="${build.classes}"
            debug="${debug}"
            deprecation="${deprecation}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            optimize="${optimize}">
 
       <classpath refid="classpath"/>
@@ -435,6 +435,10 @@
           description="-> creates the package jar file(s)">
 
     <mkdir dir="${dist.lib.pkg}"/>
+    <manifestclasspath property="jar.class.path"
+                       jarfile="${dist.lib.pkg}/${name}.jar">
+      <classpath refid="jar.classpath" />
+    </manifestclasspath>
     <jar destfile="${dist.lib.pkg}/${name}.jar"
          basedir="${build.classes}">
       <manifest>
@@ -443,16 +447,6 @@
       </manifest>
     </jar>
 
-    <!-- Sign all jar files -->
-    <antcall target="signjars"/>
-  </target>
-
-  <target name="signjars" if="sign.jars">
-    <signjar jar="${dist.lib.pkg}/${name}.jar"
-             alias="${webstart.alias}"
-             keystore="${webstart.keystore}"
-             keypass="${webstart.keypass}"
-             storepass="${webstart.storepass}"/>
   </target>
 
   <!--
@@ -827,7 +821,7 @@
              windowtitle="${Name} API"
              doctitle="${Name}"
              defaultexcludes="yes"
-             source="${source.version}"
+             encoding="cp1252"
              classpathref="classpath">
       <arg value="-Xdoclint:all,-missing"/>
 
@@ -910,7 +904,8 @@
     <javac srcdir="${tests.dir}"
            destdir="${build.tests}"
            debug="${debug}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            deprecation="${deprecation}" >
 
       <classpath refid="tests-classpath"/>
